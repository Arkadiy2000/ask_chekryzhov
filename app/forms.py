from django import forms
from django.db import models
from django.contrib.auth.models import User
from app.models import Question, Answer, Profile

class LoginForm(forms.Form):
    username = forms.CharField()
    password = forms.CharField(widget=forms.PasswordInput)

    def clean_username(self):
        username = self.cleaned_data['username']
        if ' ' in username:
            raise forms.ValidationError('need username dont contain spaces!')
        else:
            return username

    def clean_password(self):
        password = self.cleaned_data['password']
        if ' ' in password:
            raise forms.ValidationError('need password dont contain spaces!')
        else:
            return password

class QuestionForm(forms.ModelForm):
    class Meta:
        model = Question
        fields = ['title', 'body']

    def __init__(self, Profile, *args, **kwargs):
        self.profileID = Profile
        super().__init__(*args, **kwargs)
    
    def save(self, commit = True):
        question = Question(**self.cleaned_data)
        question.profileID = self.profileID
        if commit:
            question.save()
        return question

class SignupForm(forms.Form):
    username = forms.CharField()
    email = forms.CharField()
    password = forms.CharField(widget=forms.PasswordInput)
    nickname = forms.CharField()
    
    def clean_username(self):
        username = self.cleaned_data['username']
        if ' ' in username:
            raise forms.ValidationError('need username dont contain spaces!')
        else:
            return username
    
    def clean_email(self):
        email = self.cleaned_data['email']
        if '@' in email:
            return email
        else:
            raise forms.ValidationError('email dont contain @')

    def clean_password(self):
        password = self.cleaned_data['password']
        if ' ' in password:
            raise forms.ValidationError('need password dont contain spaces!')
        else:
            return password
    
    

class AnswerForm(forms.ModelForm):
    class Meta:
        model = Answer
        fields = ['body']

    def __init__(self, Profile, Question, *args, **kwargs):
        self.profileID = Profile
        self.questionID = Question
        super().__init__(*args, **kwargs)
    
    def save(self, commit = True):
        answer = Answer(**self.cleaned_data)
        answer.profileID = self.profileID
        answer.questionID = self.questionID
        if commit:
            answer.save()
        return answer
