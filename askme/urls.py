from django.contrib import admin
from django.urls import path

from app import views

urlpatterns = [
    #path('admin/', admin.site.urls)
    path('', views.index, name='index'),
    path('login/', views.login, name='login'),
    path('ask/', views.ask, name='ask'),
    path('tag/<str:tags>', views.index_tags, name='index_tags'),
    path('signup/', views.signup, name='signup'),
    path('setting/', views.setting, name='setting'),
    path('question/<int:qid>', views.question, name='question'),
    path('hot/', views.hot, name='hot'),
    path('logout/', views.logout_view, name='logout'),
    path('admin/', admin.site.urls),
    
]
