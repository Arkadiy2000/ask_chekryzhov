from django.contrib.auth.models import User
from django.contrib.contenttypes.fields import GenericForeignKey
from django.db import models

class Profile(models.Model):
    nickname = models.CharField(max_length=50, default = ' ')
    avatar = models.ImageField(default = "def.png")
    user = models.OneToOneField(User, on_delete=models.CASCADE)


    def __str__(self):
        return 'Profile: {0} Avatar: {1} Ник: {2}'.format(self.id, self.avatar, self.nickname)

class Question(models.Model):
    profileID = models.ForeignKey(Profile, on_delete = models.CASCADE)
    title = models.CharField(max_length=50)
    body = models.TextField(blank = True, null = True)
    date = models.DateTimeField(auto_now_add = True, null = True)


    def __str__(self):
        return 'id - {0} Заголовок - {1} Тело - {2} Дата - {3} Автор - {4}'.format(self.id, self.title, self.body, self.date, self.profileID.nickname)

class Answer(models.Model):
    profileID = models.ForeignKey(Profile, on_delete = models.CASCADE)
    questionID = models.ForeignKey(Question, on_delete = models.CASCADE)
    body = models.TextField(blank = True, null = True)
    date = models.DateTimeField(auto_now_add = True, null = True)


    def __str__(self):
        return 'id - {0} Тело - {1} Дата - {2} Автор - {3}'.format(self.id, self.body, self.date, self.profileID.nickname)

class Likeq(models.Model):
    CHOICES = [
        ('L', 'Like'),
        ('D', 'Dis'),
    ]
    profileID = models.ForeignKey(Profile, on_delete = models.CASCADE)
    questionID = models.ForeignKey(Question, on_delete = models.CASCADE)
    status = models.CharField(max_length = 1, choices = CHOICES)


    def __str__(self):
        return 'id - {0}'.format(self.id)

class Likea(models.Model):
    CHOICES = [
        ('L', 'Like'),
        ('D', 'Dis'),
    ]
    profileID = models.ForeignKey(Profile, on_delete = models.CASCADE)
    answerID = models.ForeignKey(Answer, on_delete = models.CASCADE)
    status = models.CharField(max_length = 1, choices = CHOICES)


    def __str__(self):
        return 'id - {0}'.format(self.id)

class Tag(models.Model):
    title = models.CharField(max_length=50)


    def __str__(self):
        return 'id - {0}  Название - {1}'.format(self.id, self.title)

class TagQuestion(models.Model):
    TagID = models.ForeignKey(Tag, on_delete = models.CASCADE)
    questionID = models.ForeignKey(Question, on_delete = models.CASCADE)


    def __str__(self):
        return ' '

# Create your models here.
