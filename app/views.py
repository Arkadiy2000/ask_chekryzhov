from django.contrib import auth
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse
from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from app import forms

from django.contrib.auth import logout
from .models import Profile, Question, Answer, Likeq, Likea, Tag, TagQuestion

from django.core.paginator import EmptyPage, InvalidPage, PageNotAnInteger, Paginator

def paginate(objects_list, request, per_page):
    page_number = request.GET.get('page')
    if (page_number == None):
        page_number = 1

    paginator = Paginator(objects_list, per_page)
    if (paginator.num_pages == 0):
        return None, None

    try:
        page = paginator.page(page_number)
    except EmptyPage:
        page = paginator.page(paginator.num_pages)
    except InvalidPage:
        page = paginator.page(1)

    return page.object_list, page

def logout_view(request):
	logout(request)
	return redirect('login')

def index(request):
	if request.user.is_authenticated:
		authterized = True
		profile = request.user.profile
	else:
		authterized = False
		profile = None
	qs = Question.objects.all().order_by('-date')
	qrs = []
	tags  = TagQuestion.objects.all()
	for q in qs:
		like_count = Likeq.objects.filter(questionID = q.id).filter(status = 'L').count()
		dis_count = Likeq.objects.filter(questionID = q.id).filter(status = 'D').count()
		ans_count = Answer.objects.filter(questionID = q.id).count()
		qrs.append({'question': q, 'rate': like_count - dis_count, 'like': like_count, 'dis': dis_count, 'ans': ans_count})
	question_page, page = paginate(qrs, request, 5)
	return render(request, 'index.html', {			
			'questions': question_page,
			'page' : page,
			'tags' : tags,
			'authterized' : authterized,
			'profile' : profile,
		})


def hot(request):
	if request.user.is_authenticated:
		authterized = True
		profile = request.user.profile
	else:
		authterized = False
		profile = None
	qs = Question.objects.all().order_by('date')
	qrs = []
	tags  = TagQuestion.objects.all()
	for q in qs:
		like_count = Likeq.objects.filter(questionID = q.id).filter(status = 'L').count()
		dis_count = Likeq.objects.filter(questionID = q.id).filter(status = 'D').count()
		ans_count = Answer.objects.filter(questionID = q.id).count()
		qrs.append({'question': q, 'rate': like_count - dis_count, 'like': like_count, 'dis': dis_count, 'ans': ans_count})
	qrs = sorted(qrs, reverse=True, key = lambda qr: qr['rate'])
	question_page, page = paginate(qrs, request, 5)
	return render(request, 'hot.html', {
		'questions': question_page,
		'page' : page,
		'tags' : tags,
		'authterized' : authterized,
		'profile' : profile,
})

def login(request):
	warning = '0'
	if request.user.is_authenticated:
		authterized = True
	else:
		authterized = False
	if request.method == 'GET':
		form = forms.LoginForm()
		ctx = {'form': form, 'authterized' : authterized, 'warning' : warning}
		return render(request, 'login.html', ctx)
	form = forms.LoginForm(data=request.POST)
	if form.is_valid():
		user = auth.authenticate(request, **form.cleaned_data)
		if user is not None:
			auth.login(request,user)
			return redirect('/')
		else:
			warning = '1'
	ctx = {'form': form, 'authterized' : authterized, 'warning' : warning}
	return render(request, 'login.html', ctx)

@login_required
def ask(request):
	if request.user.is_authenticated:
		authterized = True
		profile = request.user.profile
	else:
		authterized = False
		profile = None
	if request.method == 'GET':
		form = forms.QuestionForm(request.user.profile)
		ctx = {'form': form, 'authterized' : authterized, 'profile' : profile}
		return render(request, 'ask.html', ctx)
	form = forms.QuestionForm(request.user.profile, data=request.POST)
	if form.is_valid():
		question = form.save()
		return redirect('question', qid =question.pk)
	ctx = {'form': form, 'authterized' : authterized, 'profile' : profile}
	return render(request, 'ask.html', ctx)

def index_tags(request, tags):
	if request.user.is_authenticated:
		authterized = True
		profile = request.user.profile
	else:
		authterized = False
		profile = None
	tag = tags
	qs = Question.objects.all().order_by('date')
	qrs = []
	tags  = TagQuestion.objects.all()
	for q in qs:
		for t in tags:
			if t.questionID.id == q.id and t.TagID.title == tag:
				like_count = Likeq.objects.filter(questionID = q.id).filter(status = 'L').count()
				dis_count = Likeq.objects.filter(questionID = q.id).filter(status = 'D').count()
				ans_count = Answer.objects.filter(questionID = q.id).count()
				qrs.append({'question': q, 'rate': like_count - dis_count, 'like': like_count, 'dis': dis_count, 'ans': ans_count})
	question_page, page = paginate(qrs, request, 5)
	return render(request, 'index_tags.html', {
		'tag' : tag,
		'tags': tags,
		'questions': question_page,
		'page' : page,
		'authterized' : authterized,
		'profile' : profile,
})

def signup(request):
	warning = '0'
	if request.user.is_authenticated:
		authterized = True
	else:
		authterized = False
	if request.method == 'GET':
		form = forms.SignupForm()
		ctx = {'form': form, 'authterized' : authterized, 'warning' : warning}
		return render(request, 'signup.html', ctx)
	form = forms.SignupForm(data=request.POST)
	#print(form)
	if form.is_valid():
		u = User.objects.filter(username=form.cleaned_data['username']).count()
		if u == 0:
			user = User.objects.create_user(form.cleaned_data['username'], form.cleaned_data['email'], form.cleaned_data['password'])
			Profile.objects.create(user = user, nickname = form.cleaned_data['nickname'])
			if user is not None:
				user.save()
				auth.login(request,user)
				return redirect('/')
		else:
			print('такой юзер сущ')
			warning = '1'
	ctx = {'form': form, 'authterized' : authterized, 'warning' : warning}
	return render(request, 'signup.html', ctx)

def setting(request):
	if request.user.is_authenticated:
		authterized = True
		profile = request.user.profile
	else:
		authterized = False
		profile = None
	return render(request, 'setting.html', {
		'authterized' : authterized,
		'profile' : profile,
	})

def question(request, qid):
	if request.user.is_authenticated:
		authterized = True
		profile = request.user.profile
	else:
		authterized = False
		profile = None
	anss = Answer.objects.filter(questionID = qid).all()
	qs = Question.objects.filter(id = qid).all()
	qlike = Likeq.objects.filter(questionID = qid).filter(status = 'L').count()
	qdis = Likeq.objects.filter(questionID = qid).filter(status = 'D').count()
	qans = len(anss)
	for q in qs:
		question = ({'question': q, 'rate': qlike - qdis, 'like': qlike, 'dis': qdis, 'ans': qans})
	qtags = TagQuestion.objects.all()
	ansr = []
	for ans in anss:
		like_count = Likea.objects.filter(answerID = ans.id).filter(status = 'L').count()
		dis_count = Likea.objects.filter(answerID = ans.id).filter(status = 'D').count()
		ansr.append({'answer': ans, 'rate': like_count - dis_count, 'like': like_count, 'dis': dis_count})
	ansr = sorted(ansr, reverse=True, key = lambda anr: anr['rate'])
	answer_page, page = paginate(ansr, request, 3)

	if request.method == 'GET':
		form = forms.AnswerForm(profile, question['question'])
		ctx = {
			'answers': answer_page,
			'page' : page,
			'question' : question,
			'qtags' : qtags,
			'authterized' : authterized,
			'profile' : profile,
			'form': form,
		}
		return render(request, 'question.html', ctx)
	form = forms.AnswerForm(profile, question['question'], data=request.POST)
	if form.is_valid() and authterized:
		answer = form.save()
		return redirect('question', qid = qid)
	ctx = {
		'answers': answer_page,
		'page' : page,
		'question' : question,
		'qtags' : qtags,
		'authterized' : authterized,
		'profile' : profile,
		'form': form,
	}

	return render(request, 'question.html', ctx)
