from django.contrib import admin
from .models import Profile, Question, Answer, Likeq, Likea, Tag, TagQuestion

admin.site.register(Profile)
admin.site.register(Question)
admin.site.register(Answer)
admin.site.register(Likeq)
admin.site.register(Likea)
admin.site.register(Tag)
admin.site.register(TagQuestion)
# Register your models here.
